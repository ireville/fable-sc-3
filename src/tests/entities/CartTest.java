/**
 * @author <a href="mailto:ivzyl@edu.hse.ru"> Iryna Zyl</a>
 */

package entities;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.nio.channels.InterruptedByTimeoutException;

import static org.junit.jupiter.api.Assertions.*;

class CartTest {
    private static Cart cart;

    @BeforeEach
    void setUp() {
        cart = new Cart();
    }

    @Test
    void setX() {
        assertEquals(0, cart.getX());
        cart.setX(3);
        assertEquals(3, cart.getX());

        cart.setX(-100);
        assertEquals(-100, cart.getX());
        cart.setX(678);
        assertEquals(678, cart.getX());
        cart.setX(3.9);
        assertEquals(3.9, cart.getX());
        cart.setX(1.2);
        assertEquals(1.2, cart.getX());
    }

    @Test
    void setY() {
        assertEquals(0, cart.getY());

        cart.setY(5);
        assertEquals(5, cart.getY());

        cart.setY(-101);
        assertEquals(-101, cart.getY());

        cart.setY(378);
        assertEquals(378, cart.getY());

        cart.setY(2.9);
        assertEquals(2.9, cart.getY());

        cart.setY(1.2);
        assertEquals(1.2, cart.getY());
    }

    @Test
    void getX() {
        assertEquals(0, cart.getX());
        cart.setX(3);
        assertEquals(3, cart.getX());

        cart.setX(-100);
        assertEquals(-100, cart.getX());
        cart.setX(678);
        assertEquals(678, cart.getX());
        cart.setX(3.9);
        assertEquals(3.9, cart.getX());
        cart.setX(1.2);
        assertEquals(1.2, cart.getX());
    }

    @Test
    void getY() {
        assertEquals(0, cart.getY());

        cart.setY(5);
        assertEquals(5, cart.getY());

        cart.setY(-101);
        assertEquals(-101, cart.getY());

        cart.setY(378);
        assertEquals(378, cart.getY());

        cart.setY(2.9);
        assertEquals(2.9, cart.getY());

        cart.setY(1.2);
        assertEquals(1.2, cart.getY());
    }

    @Test
    void move() {
        try {
            Creature creature = new Creature("Swan", 60, cart);
            double oldX = cart.getX(), oldY = cart.getY();
            cart.move(creature);
            assertEquals(getNear(creature.getS() * Math.cos(Math.toRadians(60))), getNear(cart.getX() - oldX));
            assertEquals(getNear(creature.getS() * Math.sin(Math.toRadians(60))), getNear(cart.getY() - oldY));

            creature = new Creature("Crayfish", 99, cart);
            oldX = cart.getX();
            oldY = cart.getY();
            cart.move(creature);
            assertEquals(getNear(creature.getS() * Math.cos(Math.toRadians(99))), getNear(cart.getX() - oldX));
            assertEquals(getNear(creature.getS() * Math.sin(Math.toRadians(99))), getNear(cart.getY() - oldY));

            creature = new Creature("Pike", 2, cart);
            oldX = cart.getX();
            oldY = cart.getY();
            cart.move(creature);
            assertEquals(getNear(creature.getS() * Math.cos(Math.toRadians(2))), getNear(cart.getX() - oldX));
            assertEquals(getNear(creature.getS() * Math.sin(Math.toRadians(2))), getNear(cart.getY() - oldY));

            creature = new Creature("Someone", -50, cart);
            oldX = cart.getX();
            oldY = cart.getY();
            cart.move(creature);
            assertEquals(getNear(creature.getS() * Math.cos(Math.toRadians(-50))), getNear(cart.getX() - oldX));
            assertEquals(getNear(creature.getS() * Math.sin(Math.toRadians(-50))), getNear(cart.getY() - oldY));

            Creature creature2 = new Creature("Someone2", -12, cart);
            Thread.currentThread().interrupt();
            assertThrows(Exception.class, () -> cart.move(creature2));
        } catch (Exception ignored) {
        }
    }

    /**
     * "Округляет" число с заданной точностью.
     *
     * @param x Исходное число.
     * @return Новое число.
     */
    private long getNear(double x) {
        return (long) (x * 1000000000000.0);
    }
}