/**
 * @author <a href="mailto:ivzyl@edu.hse.ru"> Iryna Zyl</a>
 */
package entities;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CreatureTest {
    private static Creature creature;
    private static Cart cart;

    @BeforeAll
    static void createCart() {
        cart = new Cart();
    }

    @Test
    void getName() {
        creature = new Creature("Hello", 1, cart);
        assertEquals("Hello", creature.getName());

        creature = new Creature("Swan", 2, cart);
        assertEquals("Swan", creature.getName());

        creature = new Creature("Crayfish", 300, cart);
        assertEquals("Crayfish", creature.getName());

        creature = new Creature("Pike", 189, cart);
        assertEquals("Pike", creature.getName());
    }

    @Test
    void getAlpha() {
        creature = new Creature("Hello", 1, cart);
        assertEquals(1, creature.getAlpha());

        creature = new Creature("Swan", 2, cart);
        assertEquals(2, creature.getAlpha());

        creature = new Creature("Crayfish", 300, cart);
        assertEquals(300, creature.getAlpha());

        creature = new Creature("Pike", 189, cart);
        assertEquals(189, creature.getAlpha());

        creature = new Creature("Pike", -67, cart);
        assertEquals(-67, creature.getAlpha());
    }

    @Test
    void calculateXShift() {
        creature = new Creature("Swan", 60, cart);
        assertEquals(creature.getS() * Math.cos(Math.toRadians(60)), creature.calculateXShift());

        creature = new Creature("Crayfish", 100, cart);
        assertEquals(creature.getS() * Math.cos(Math.toRadians(100)), creature.calculateXShift());

        creature = new Creature("Pike", 2, cart);
        assertEquals(creature.getS() * Math.cos(Math.toRadians(2)), creature.calculateXShift());

        creature = new Creature("Someone", -50, cart);
        assertEquals(creature.getS() * Math.cos(Math.toRadians(-50)), creature.calculateXShift());
    }

    @Test
    void calculateYShift() {
        creature = new Creature("Swan", 60, cart);
        assertEquals(creature.getS() * Math.sin(Math.toRadians(60)), creature.calculateYShift());

        creature = new Creature("Crayfish", 100, cart);
        assertEquals(creature.getS() * Math.sin(Math.toRadians(100)), creature.calculateYShift());

        creature = new Creature("Pike", 2, cart);
        assertEquals(creature.getS() * Math.sin(Math.toRadians(2)), creature.calculateYShift());

        creature = new Creature("Someone", -50, cart);
        assertEquals(creature.getS() * Math.sin(Math.toRadians(-50)), creature.calculateYShift());
    }
}