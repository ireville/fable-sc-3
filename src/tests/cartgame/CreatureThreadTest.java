/**
 * @author <a href="mailto:ivzyl@edu.hse.ru"> Iryna Zyl</a>
 */

package cartgame;

import entities.Cart;
import entities.Creature;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CreatureThreadTest {

    @Test
    void run() {
        assertDoesNotThrow(this::makeSituation);
        assertDoesNotThrow(this::makeLongSituation);
        assertDoesNotThrow(this::makeVeryLongSituation);

        assertDoesNotThrow(this::makeSituation);
        assertDoesNotThrow(this::makeLongSituation);
        assertDoesNotThrow(this::makeVeryLongSituation);

        assertDoesNotThrow(this::makeSituation);
        assertDoesNotThrow(this::makeLongSituation);
        assertDoesNotThrow(this::makeVeryLongSituation);
    }

    /**
     * Создаёт ситуацию работы и прерывания потока существа.
     */
    private void makeSituation() {
        long timer = 0;
        CreatureThread thread = new CreatureThread(new Creature("Swan", 60, new Cart()), timer);

        thread.start();
        Thread.yield();
        thread.interrupt();
    }

    /**
     * Создаёт ситуацию более долгой работы и прерывания потока существа.
     */
    private void makeLongSituation() {
        long timer = 0;
        CreatureThread thread = new CreatureThread(new Creature("Swan", 60, new Cart()), timer);

        thread.start();
        try {
            Thread.sleep(20);
        } catch (InterruptedException ignored) {
        }

        thread.interrupt();
    }

    /**
     * Создаёт ещё более долгую ситуацию работы и прерывания потока существа.
     */
    private void makeVeryLongSituation() {
        long timer = 0;
        CreatureThread thread = new CreatureThread(new Creature("Swan", 60, new Cart()), timer);

        thread.start();
        try {
            Thread.sleep(1000);
        } catch (InterruptedException ignored) {
        }

        thread.interrupt();
    }
}