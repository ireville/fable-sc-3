/**
 * @author <a href="mailto:ivzyl@edu.hse.ru"> Iryna Zyl</a>
 */

package cartgame;

import entities.Cart;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ProgramTest {

    @Test
    void main() {
        Program.MAIN_SLEEP = 1100;
        assertDoesNotThrow(() -> Program.main(new String[]{"1", "2"}));
        assertDoesNotThrow(() -> Program.main(new String[]{"he", "llo"}));
        assertDoesNotThrow(() -> {
            Thread tr = new Thread(() -> Program.main(new String[0]));
            tr.start();
            tr.interrupt();
        });
        assertDoesNotThrow(() -> {
            Thread tr = new Thread(() -> Program.main(new String[0]));
            tr.start();
            Thread.sleep(50);
            tr.interrupt();
        });

        Program.MAIN_SLEEP = 2100;
        assertDoesNotThrow(() -> Program.main(new String[]{"3", "4"}));
    }

    @Test
    void tryParse() {
        assertTrue(Program.tryParse("1"));
        assertTrue(Program.tryParse("1.8173"));
        assertTrue(Program.tryParse("123.1823"));
        assertTrue(Program.tryParse("-1237.1238"));
        assertTrue(Program.tryParse("-13978.328722222223231231"));

        assertFalse(Program.tryParse("hello"));
        assertFalse(Program.tryParse("883j212"));
        assertFalse(Program.tryParse("29832,8237"));
        assertFalse(Program.tryParse("kikik"));
        assertFalse(Program.tryParse("1232,123982"));
        assertFalse(Program.tryParse("1273687.129837\\"));
    }

    @Test
    void readInputData() {
        Cart cart = new Cart();
        assertTrue(Program.readInputData(new String[]{"1", "2"}, cart));
        assertEquals(1, cart.getX());
        assertEquals(2, cart.getY());

        assertTrue(Program.readInputData(new String[]{"1232", "1232"}, cart));
        assertEquals(1232, cart.getX());
        assertEquals(1232, cart.getY());

        assertTrue(Program.readInputData(new String[]{"1736.17238271", "87368.18717238"}, cart));
        assertEquals(1736.17238271, cart.getX());
        assertEquals(87368.18717238, cart.getY());

        assertTrue(Program.readInputData(new String[]{"923483.1528", "92726178.12382"}, cart));
        assertEquals(923483.1528, cart.getX());
        assertEquals(92726178.12382, cart.getY());

        assertTrue(Program.readInputData(new String[]{"1263912.82817", "192392.1126382"}, cart));
        assertEquals(1263912.82817, cart.getX());
        assertEquals(192392.1126382, cart.getY());

        cart = new Cart();

        assertFalse(Program.readInputData(new String[]{"hello", "bye"}, cart));
        assertEquals(0, cart.getX());
        assertEquals(0, cart.getY());

        assertFalse(Program.readInputData(new String[]{"hello", "1"}, cart));
        assertFalse(Program.readInputData(new String[]{"123123.1923\\", "128392"}, cart));
        assertFalse(Program.readInputData(new String[]{"198239.913j198287", "1239"}, cart));
        assertFalse(Program.readInputData(new String[]{"19823", "123.928734o9"}, cart));
        assertEquals(0, cart.getX());
        assertEquals(0, cart.getY());
    }
}