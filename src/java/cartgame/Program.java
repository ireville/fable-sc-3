/**
 * @author <a href="mailto:ivzyl@edu.hse.ru"> Iryna Zyl</a>
 */

/* Для справки: при вводе данных программы в качестве координат принимаются вещественные числа. Если не введено чисел -
 * координаты по умолчанию будут равны 0, если вводится одно число - то оно будет записано как значение координаты х.
 * Если вводится более двух вещественных чисел, то первые два будут взяты как значения координат х и y соответственно.
 */

package cartgame;

import entities.Cart;
import entities.Creature;
import log.Log;
import log.Notification;

import static java.lang.Double.parseDouble;

public class Program {
    // Время, на котрое засыпает программа.
    // По дефолту замеряем чуть меньше, чем 25 секунд, чтобы как раз как можно ближе к 25-ой секунде прервать потоки.
    static int MAIN_SLEEP = 24999;

    public static void main(String[] args) {
        // Создаём тележку
        Cart cart = new Cart();

        // Проверяем и парсим входные данные.
        if (!readInputData(args, cart)) {
            System.out.println("Incorrect input data. ");
            return;
        }

        // Создаём существ.
        Creature[] creatures = new Creature[3];
        creatures[0] = new Creature("Swan", 60, cart);
        creatures[1] = new Creature("Crayfish", 180, cart);
        creatures[2] = new Creature("Pike", 300, cart);
        Log.creaturesInfo(creatures);

        // Создаём потоки и начинаем отсчёт времени.
        CreatureThread[] threads = new CreatureThread[3];
        long timer = System.currentTimeMillis();

        // Начинаем каждые 2 секунды уведомлять о положении тележки.
        Notification notify = new Notification(cart, timer);
        notify.start();

        // Создаём и запускаем потоки.
        for (int i = 0; i < 3; i++) {
            threads[i] = new CreatureThread(creatures[i], timer);
            threads[i].start();
        }

        // Засыпаем на ~25 секунд.
        try {
            Thread.sleep(MAIN_SLEEP);
        } catch (InterruptedException ie) {
            System.out.println("Main was interrupted!! ");
        }

        // Прерываем потоки существ.
        for (int i = 0; i < 3; i++) {
            threads[i].interrupt();
        }

        // Завершаем вывод о положении тележки.
        notify.interrupt();
    }

    /**
     * Пытается распарсить строку в вещенственное число.
     *
     * @param value Строка со входными данными.
     * @return Число, которое содержалось во входной строке.
     */
    static boolean tryParse(String value) {
        try {
            parseDouble(value);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * Проверяет корректность входных данных и преобразует их в нужный тип.
     *
     * @param args Входные данные.
     * @return Истина, если даннные корректны, ложь, если нет.
     */
    static boolean readInputData(String[] args, Cart cart) {
        double[] xy = {0, 0};

        for (int i = 0; i < Math.min(2, args.length); i++) {
            if (!tryParse(args[i])) {
                return false;
            }

            xy[i] = Double.parseDouble(args[i]);
        }

        cart.setX(xy[0]);
        cart.setY(xy[1]);

        return true;
    }
}
