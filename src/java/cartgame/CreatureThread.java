/**
 * @author <a href="mailto:ivzyl@edu.hse.ru"> Iryna Zyl</a>
 */

package cartgame;

import entities.Creature;
import log.Log;

import java.nio.channels.InterruptedByTimeoutException;
import java.util.Random;

public class CreatureThread extends Thread {
    private static final int MIN_SLEEP_TIME = 1000, MAX_SLEEP_TIME = 5000;
    private static final Random RND = new Random();
    private final Creature creature;
    private final long timer;

    public CreatureThread(Creature creature, long timer) {
        this.creature = creature;
        this.timer = timer;
    }

    @Override
    public void run() {
        Log.awakened(timer, creature);
        int sleepyTime; // Время, на которое будет засыпать поток.
        boolean interWhileSleeping = false;

        while (!isInterrupted()) {
            try {
                // Сдвигаем тележку.
                creature.cart.move(creature);

                Log.movedCart(timer, creature);

                // Засыпаем.
                sleepyTime = RND.nextInt(MAX_SLEEP_TIME - MIN_SLEEP_TIME) + MIN_SLEEP_TIME;
                Log.willSleep(timer, creature, sleepyTime);

                Thread.sleep(sleepyTime);
            } catch (InterruptedByTimeoutException ite) {
                // Сообщаем, что нас прервали в очереди в метод move.
                Log.interrMove(timer, creature);

                interWhileSleeping = true;
                Thread.currentThread().interrupt();
            } catch (InterruptedException ie) {
                // Сообщаем, что нас прервали во время сна.
                Log.interrSleep(timer, creature);

                interWhileSleeping = true;
                Thread.currentThread().interrupt();
            }
        }
        // Сообщаем, что нас прервали.
        if (!interWhileSleeping) {
            Log.interr(timer, creature);
        }
    }
}
