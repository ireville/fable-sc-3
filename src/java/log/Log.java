/**
 * @author <a href="mailto:ivzyl@edu.hse.ru"> Iryna Zyl</a>
 */

package log;

import entities.Cart;
import entities.Creature;

/**
 * Класс для вывода сообщений.
 */
public class Log {
    /**
     * Выводит информацию о том, что существо "проснулось" и приступило к процессу движения тележки.
     *
     * @param timer    Время начала общего процесса движения тележки.
     * @param creature Существво.
     */
    public static void awakened(long timer, Creature creature) {
        System.out.printf("[Timer at %.3f]: " + creature.getName() + " awakened!" +
                System.lineSeparator(), (System.currentTimeMillis() - timer) / 1000.0);
    }

    /**
     * Выводит информацию о том, что существо подвинуло тележку.
     *
     * @param timer    Время начала общего процесса движения тележки.
     * @param creature Существво.
     */
    public static void movedCart(long timer, Creature creature) {
        System.out.printf(System.lineSeparator() + "[Timer at %.3f]: " + creature.getName() + " moved cart. " +
                System.lineSeparator(), (System.currentTimeMillis() - timer) / 1000.0);
    }

    /**
     * Сообщает о том, на какое время засыпает существо.
     *
     * @param timer      Время начала общего процесса движения тележки.
     * @param creature   Существо.
     * @param sleepyTime Время, на которое оно уснёт.
     */
    public static void willSleep(long timer, Creature creature, int sleepyTime) {
        System.out.printf("[Timer at %.3f]: " + creature.getName() + " will sleep for " + sleepyTime +
                " milliseconds. " + System.lineSeparator(), (System.currentTimeMillis() - timer) / 1000.0);
    }

    /**
     * Сообщает о том, что поток (представляющий двигающее существо) был прерван во время сна.
     *
     * @param timer    Время начала общего процесса движения тележки.
     * @param creature Существо.
     */
    public static void interrSleep(long timer, Creature creature) {
        System.out.printf("[Timer at %.3f]: " + creature.getName() + " was interrupted while sleeping. " +
                System.lineSeparator(), (System.currentTimeMillis() - timer) / 1000.0);
    }

    /**
     * Сообщает о том, что поток (представляющий двигающее существо) был прерван во время ожидания передвижения.
     *
     * @param timer    Время начала общего процесса движения тележки.
     * @param creature Существо.
     */
    public static void interrMove(long timer, Creature creature) {
        System.out.printf("[Timer at %.3f]: " + creature.getName() + " was interrupted before moving. " +
                System.lineSeparator(), (System.currentTimeMillis() - timer) / 1000.0);
    }

    /**
     * Сообщает о том, что поток (представляющий двигающее существо) был прерван во время работы.
     *
     * @param timer    Время начала общего процесса движения тележки.
     * @param creature Существо.
     */
    public static void interr(long timer, Creature creature) {
        System.out.printf("[Timer at %.3f]: " + creature.getName() + " was interrupted while running. " +
                System.lineSeparator(), (System.currentTimeMillis() - timer) / 1000.0);
    }

    /**
     * Выводит информацию о существах.
     *
     * @param creatures Существа.
     */
    public static void creaturesInfo(Creature[] creatures) {
        for (Creature creature : creatures) {
            System.out.printf(creature.getName() + " created. Alpha: " + creature.getAlpha() + ", s: %.4f" +
                    System.lineSeparator(), creature.getS());
        }
        System.out.println();
    }

    /**
     * Выводит итоговое положние тележки.
     *
     * @param cart Тележка.
     */
    public static void results(Cart cart) {
        System.out.printf(System.lineSeparator() + "~~~~~~~~~~~~~~~~~~" + System.lineSeparator() +
                "Result cart position: x = %.2f, y = %.2f. " + System.lineSeparator() + "~~~~~~~~~~~~~~~~~~" +
                System.lineSeparator(), cart.getX(), cart.getY());
        printCart();
    }

    /**
     * Рисует тележку :)
     */
    private static void printCart() {
        System.out.println("                                                 ___");
        System.out.println("                                                  \\\\");
        System.out.println("   \\                                               \\\\");
        System.out.println("    \\                                               \\\\");
        System.out.println(" \t\\                                                \\\\");
        System.out.println(" \t|-------------------------------------------------\\\\");
        System.out.println("\t|---------------------------------------------------\\");
        System.out.println("\t|                      CART                          \\");
        System.out.println("\t|-----------------------------------------------------\\");
        System.out.println("\t|------________--------------------________------------\\");
        System.out.println("\t|     /         \\                 /         \\\t        |");
        System.out.println("\t|    / .   |   ..\\---------------/ .   |   ..\\----------|");
        System.out.println("\t|---|..   \\|/   ..|-------------|..   \\|/   ..|---------|");
        System.out.println("\t|---|..   /|\\   ..|-------------|..   /|\\   ..|---------|");
        System.out.println("\t     \\..   |   ../               \\..   |   ../");
        System.out.println("\t      \\ ________/                 \\ ________/");

    }
}
