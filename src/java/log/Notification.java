/**
 * @author <a href="mailto:ivzyl@edu.hse.ru"> Iryna Zyl</a>
 */

package log;

import entities.Cart;

/**
 * Класс, описывающий уведомление о положении тележки.
 */
public class Notification extends Thread {
    private final Cart cart;
    private final long timer;

    public Notification(Cart cart, long timer) {
        this.cart = cart;
        this.timer = timer;
    }

    public void run() {
        while ((System.currentTimeMillis() - timer) / 1000 < 25 && !isInterrupted()) {
            // Синхронизируемся по флагу, чтобы не позволить координатам измениться, пока мы их получаем.
            synchronized (cart.notificationFlag) {
                System.out.printf("-----------------" + System.lineSeparator() +
                                "[Timer at %3.3f]: Cart at x = %.2f, y = %.2f. " + System.lineSeparator() +
                                "-----------------" + System.lineSeparator(),
                        (System.currentTimeMillis() - timer) / 1000.0, cart.getX(), cart.getY());
            }
            try {
                Thread.sleep(2000);
            } catch (InterruptedException ie) {
                break;
            }
        }

        Log.results(cart);
    }
}
