/**
 * @author <a href="mailto:ivzyl@edu.hse.ru"> Iryna Zyl</a>
 */

package entities;

import java.nio.channels.InterruptedByTimeoutException;

public class Cart {
    // Флаг, по которому будет происходить синхронизация для вывода координат.
    public final Object notificationFlag = new Object();

    // Координаты тележки.
    private double x, y;

    /**
     * Предоставляет х координату тележки.
     *
     * @return х координата.
     */
    public double getX() {
        return x;
    }

    /**
     * Задаёт значение х координате тележки.
     *
     * @param x Новое значение х координаты.
     */
    public void setX(double x) {
        this.x = x;
    }

    /**
     * Предоставляет y координату тележки.
     *
     * @return y координата.
     */
    public double getY() {
        return y;
    }

    /**
     * Задаёт значение y координате тележки.
     *
     * @param y Новое значение y координаты.
     */
    public void setY(double y) {
        this.y = y;
    }

    /**
     * Реализует движение тележки.
     *
     * @param creature Существо, которое двигает тележку.
     */
    public synchronized void move(Creature creature) throws InterruptedByTimeoutException, InterruptedException {
        if (Thread.currentThread().isInterrupted()) {
            throw new InterruptedByTimeoutException();
        }

        // Предполагаем, что сдвиг тележки занимает секунду.
        Thread.sleep(1000);

        // Синхронизируем, чтобы никто не мог получить доступ к координатам, пока мы их меняем.
        synchronized (notificationFlag) {
            setX(getX() + creature.calculateXShift());
            setY(getY() + creature.calculateYShift());
        }
    }
}
