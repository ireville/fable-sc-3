/**
 * @author <a href="mailto:ivzyl@edu.hse.ru"> Iryna Zyl</a>
 */

package entities;

import java.util.Random;

public class Creature {
    public final Cart cart; // Тележка, которую будет толкать существо.
    private final int alpha;
    private final double s;
    private final String name; // Имя существа.

    public Creature(String name, int aNum, Cart cart) {
        this.name = name;
        this.alpha = aNum;
        this.cart = cart;

        Random rnd = new Random();
        s = rnd.nextInt(8) + 1 + rnd.nextDouble();
    }

    /**
     * Предоставляет имя существа.
     *
     * @return Имя существа.
     */
    public String getName() {
        return name;
    }

    /**
     * Предоставляет угол существа.
     *
     * @return Угол существа.
     */
    public int getAlpha() {
        return alpha;
    }

    /**
     * Предоставляет параметр s существа.
     *
     * @return Параметр s существа.
     */
    public double getS() {
        return s;
    }

    /**
     * Считает сдвиг по оси х для тележки.
     *
     * @return То, насколько должна сдвинуться тележка по х.
     */
    public double calculateXShift() {
        double a = alpha * Math.PI / 180;
        return s * Math.cos(a);
    }

    /**
     * Считает сдвиг по оси y для тележки.
     *
     * @return То, насколько должна сдвинуться тележка по y.
     */
    public double calculateYShift() {
        double a = alpha * Math.PI / 180;
        return s * Math.sin(a);
    }
}
